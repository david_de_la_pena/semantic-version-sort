const compareSemVer = require('../app');
const assert = require('assert').strict;

describe("Compare '1.0.0' and '1.0.0'", function () {
    it("Should be equal to 0", function () {
        assert.equal(compareSemVer('1', '1'), 0);
    });
});
describe("Compare '1.0.0' and '1.0.1'", function () {
    it("Should be equal to -1", function () {
        assert.equal(compareSemVer('1.0.0', '1.0.1'), -1);
    });
});
describe("Compare '1.0.1' and '1.0.0'", function () {
    it("Should be equal to 1", function () {
        assert.equal(compareSemVer('1.0.1', '1.0.0'), 1);
    });
});
describe("Compare '1.0.0' and '1.0.12'", function () {
    it("Should be equal to -12", function () {
        assert.equal(compareSemVer('1.0.0', '1.0.12'), -12);
    });
});

describe("Compare '1.0.12' and '1.0.0'", function () {
    it("Should be equal to 12", function () {
        assert.equal(compareSemVer('1.0.12', '1.0.0'), 12);
    });
});
describe("Compare '1.0.0' and '1.12.0'", function () {
    it("Should be equal to -12", function () {
        assert.equal(compareSemVer('1.0.0', '1.12.0'), -12);
    });
});

describe(`Semantic version test`, function () {
    let versions = ['1.0.0', '3.1.2', '0.9.0-rc.12'];
    let orderedVersions = ['0.9.0-rc.12', '1.0.0', '3.1.2'];
    describe(`Sorting ${versions}`, function () {
        it(`Should be equal to ${orderedVersions}`, function () {
            assert.deepEqual(versions.sort(compareSemVer), orderedVersions);
        });
    });
});


describe(`Semantic version test`, function () {
    let versions = ['1.0.0', '3.1.2', '0.9.0-rc.12', '1.0.0-beta.2', '0.9.0-rc.1', '0.9.0-beta', '0.9.1-rc', '10.0.0', '0.9.0-alpha', '1.0.0-beta.11', '0.9.0-rc', '3.10.0',
        '3.1.2', '3.1.1', '10.0.1', '1.0.0-beta', '1.100.20', '1.0.0-beta.1', '0.8.0-beta', '1.1.10', '1.0.0-rc.1',
        '1.1.0', '1.0.0-alpha.beta', '1.0.0-alpha.1', '1.100.2', '1.0.0-alpha', '0.8.0-alpha.1'];
    let orderedVersions = [
        '0.8.0-alpha.1', '0.8.0-beta',
        '0.9.0-alpha', '0.9.0-beta',
        '0.9.0-rc.1', '0.9.0-rc.12',
        '0.9.0-rc', '0.9.1-rc',
        '1.0.0-alpha.1', '1.0.0-alpha.beta',
        '1.0.0-alpha', '1.0.0-beta.1',
        '1.0.0-beta.2', '1.0.0-beta.11',
        '1.0.0-beta', '1.0.0-rc.1',
        '1.0.0', '1.1.0',
        '1.1.10', '1.100.2',
        '1.100.20', '3.1.1',
        '3.1.2', '3.1.2',
        '3.10.0', '10.0.0',
        '10.0.1'
    ];
    describe(`Sorting ${versions}`, function () {
        it(`Should be equal to ${orderedVersions}`, function () {
            assert.deepEqual(versions.sort(compareSemVer), orderedVersions);
        });
    });
});






describe(`Semantic version test`, function () {
    let values = ['2', '1'];
    let valuesOrdered = ['1', '2'];
    describe(`Sorting ${values}`, function () {
        it(`Should be equal to ${valuesOrdered}`, function () {
            assert.deepEqual(values.sort(compareSemVer), valuesOrdered);
        });
    });
});
values = ['6', '7'];

/*versionSort = function (a, b) {
    let ret = 0;
    let aSplit = a.split(".");
    let bSplit = b.split(".");

    let majorVersionDiff = parseInt(aSplit[0]) - parseInt(bSplit[0]);
    let minorVersionDiff = parseInt(aSplit[1]) - parseInt(bSplit[1]);
    let patch = parseInt(aSplit[2]) - parseInt(bSplit[2]);

    if (!majorVersionDiff) {
        if (!minorVersionDiff) {
            ret = patch;
        }
        else
            ret = minorVersionDiff;
    }
    else
        ret = majorVersionDiff;

    return ret;
};

getRandomVersionNumber = function () {
    majorVersionDiff = Math.round(Math.random() * 10 + 1).toString();
    minorVersionDiff = Math.round(Math.random() * 10).toString();
    patch = Math.round(Math.random() * 100).toString();
    let ret = majorVersionDiff + "." + minorVersionDiff + "." + patch;
    return ret.toString();
};

getRandomVersionsArray = function (arrayLength) {
    let versions = []
    for (i = 0; i, arrayLength; i++) {
        versions.push(getRandomVersionNumber())
    }
    return versions
}
*/
let versions = []
//versions = getRandomVersionsArray(10000)

/*
console.time('versions sort by string')
versions.sort()
//console.timeEnd('versions sort by string')

//versions = getRandomVersionsArray(10000)

console.time('versions sort by number')

versions = ['10', '3.10', '3.1.2', '3.1.1', '10.2.1', '1.100.20', '1.1.10', '1.1', '1', '1.100.2',]

versions = ['3.1.2', '10', '3.10', '3.1.2', '3.1.1', '10.2.1', '1.100.20', '1.1.10', '1.1', '1', '1.100.2',]
console.log("input: " + versions.join(', '))

versions.sort(function (a, b) {
    let aSplit = a.split(".")
    let bSplit = b.split(".")

    let majorVersionDiff = parseInt(aSplit[0]) - parseInt(bSplit[0])
    if (majorVersionDiff == 0) {
        if (aSplit[1] == undefined)
            return -1
        else if (bSplit[1] == undefined)
            return majorVersionDiff

        let minorVersionDiff = parseInt(aSplit[1]) - parseInt(bSplit[1])

        if (minorVersionDiff == 0) {
            if (aSplit[2] == undefined)
                return -1
            else if (bSplit[2] == undefined)
                return minorVersionDiff

            return parseInt(aSplit[2]) - parseInt(bSplit[2])
        } else
            return minorVersionDiff
    } else
        return majorVersionDiff
})

console.log("output: " + versions.join(', '))


*/






function compareSemVer(a, b) {
    // Ref.: https://semver.org/

    // Split string on first call
    if (!Array.isArray(a) || !Array.isArray(b))
        return compareSemVer(a.split(/[.\-+]+/), b.split(/[.\-+]+/));

    // Check remaining elements
    if (a.length == 0 && b.length == 0) return 0;

    // 11.4.4 A larger set of pre-release fields has a higher precedence 
    // than a smaller set, if all of the preceding identifiers are equal.
    if (a.length == 0) return 1;
    if (b.length == 0) return -1;

    // If first elements are equal
    if (a[0] === b[0]) {
        // Remove first element each, then go recursive
        a.shift();
        b.shift();
        return compareSemVer(a, b);
    } else {
        // String value to compare (pre-release or build version)
        if (isNaN(a[0]) || isNaN(b[0])) {
            if (a[0] > b[0]) return 1;
            if (a[0] < b[0]) return -1;
        } else {
            // Number comparison
            return parseInt(a[0]) - parseInt(b[0]);
        }
    }
}



module.exports = compareSemVer;


versions = ['1.0.0', '3.1.2', '0.9.0-rc.12', '1.0.0-beta.2', '0.9.0-rc.1', '0.9.0-beta', '0.9.1-rc', '10.0.0', '0.9.0-alpha', '1.0.0-beta.11', '0.9.0-rc', '3.10.0',
    '3.1.2', '3.1.1', '10.0.1', '1.0.0-beta', '1.100.20', '1.0.0-beta.1', '0.8.0-beta', '1.1.10', '1.0.0-rc.1',
    '1.1.0', '1.0.0-alpha.beta', '1.0.0-alpha.1', '1.100.2', '1.0.0-alpha', '0.8.0-alpha.1']
console.log(versions)
versions.sort(compareSemVer);
console.log(versions)


/*
versions = ['1.0.0', '3.1.2', '0.9.0-rc.12', '1.0.0-beta.2', '0.9.0-rc.1', '0.9.0-beta', '0.9.1-rc', '10.0.0', '0.9.0-alpha', '1.0.0-beta.11', '0.9.0-rc', '3.10.0',
    '3.1.2', '3.1.1', '10.0.1', '1.0.0-beta', '1.100.20', '1.0.0-beta.1', '0.8.0-beta', '1.1.10', '1.0.0-rc.1',
    '1.1.0', '1.0.0-alpha.beta', '1.0.0-alpha.1', '1.100.2', '1.0.0-alpha', '0.8.0-alpha.1']

versions.sort(function (a, b) {
    // Semantic versions sorting: https://semver.org/

    // Split version core and pre-release, eg: '1.0.0-beta.11' > ['1.0.0','beta.11']
    let aPreRelease = a.split("-")
    let bPreRelease = b.split("-")

    // Compare version core (major, minor, and patch)
    let releaseOrder = compareSemVer(aPreRelease[0].split("."), bPreRelease[0].split("."))

    // If major, minor, and patch are equal check pre-release version
    if (releaseOrder == 0) {
        if (aPreRelease[1] == undefined && bPreRelease[1] == undefined) return 0

        // 11.3 When major, minor, and patch are equal, a pre-release version has lower precedence than a normal version
        if (aPreRelease[1] == undefined) return 1
        if (bPreRelease[1] == undefined) return -1

        // Compare pre-release
        return compareSemVer(aPreRelease[1].split("."), bPreRelease[1].split("."))
    } else
        return releaseOrder
})
*/





//console.log("sorted versions: ")
//console.log(versions)


/*
versions = [ '1.0.0', '3.1.2', '1.0.0-beta.2','10.0.0', '1.0.0-beta.11', '3.10.0',
    '3.1.2', '3.1.1', '10.2.1', '1.0.0-beta', '1.100.20', '1.1.10', '1.0.0-rc.1',
    '1.1.0', '1.0.0-alpha.beta', '1.0.0', '1.0.0-alpha.1', '1.100.2', '1.0.0-alpha']
console.log("input: " + versions.join(', '))

versions.sort(function (a, b) {
    // Semantic version format expected: https://semver.org/
    let aPreRelease = a.split("-")
    let bPreRelease = b.split("-")

    let aSplit = aPreRelease[0].split(".")
    let bSplit = bPreRelease[0].split(".")

    let majorVersionDiff = parseInt(aSplit[0]) - parseInt(bSplit[0])

    if (majorVersionDiff == 0) {
        let minorVersionDiff = parseInt(aSplit[1]) - parseInt(bSplit[1])

        if (minorVersionDiff == 0) {
            let patchVersionDiff = parseInt(aSplit[2]) - parseInt(bSplit[2])

            if (patchVersionDiff == 0) {
                if (aPreRelease[1] == undefined) return 1
                if (bPreRelease[1] == undefined) return -1

                let aPRSplit = aPreRelease[1].split('.')
                let bPRSplit = bPreRelease[1].split('.')

                let prFirst = parseInt(aPRSplit[0]) - parseInt(aPRSplit[0])



            } else
                return patchVersionDiff
        } else
            return minorVersionDiff
    } else
        return majorVersionDiff
})

//console.log("output: " + versions.join(', '))
console.log(versions)


//console.timeEnd('versions sort by number')
*/